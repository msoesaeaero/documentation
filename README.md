# README #

Documentation and notes for the SAE Aero CE senior design team (2015-2016).

### What is this repository for? ###

* Deliverables in the form of reports
* Notes for meetings such as agendas or minutes
* Any internal documents that do not have a place in another SAE Aero CE repository

### How do I get set up? ###

Most documents in this repository are in .docx format, using Microsoft Word 2013.


### Special Rules ###

* As these are Word .docx files, they may not work beautifully with the merge tools provided by git.
    * Be careful when pushing changes to make sure any changed files during a merge can still be opened without issue.
    * If there is a problem, either revert the merge or manually replace the complete file, forcing git to delete the old version (pre-merge) to a completely new file (post-merge).
* As with any document created by the MSOE SAE Aero team, documents committed here should be checked for simple spelling and grammar errors.
* Please try to maintain a level of professionalism in your documents. The senior design classes are meant to help us transition to post-education work in the industry where professionalism is required nearly 100% of the time.