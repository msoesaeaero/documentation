# FPV Tech Research

## Camera
### Go Pro  
The Go Pro markets itself as being able to do anything and there are numerous quad copters which utilize the Go Pro wirelessly, however due to the resolution it would require a high bandwidth wireless connection which we don't have the luxury of at longer ranges. WiFi would be the preferred means of communication but that's too short of a range for our needs.   Expensive.  
Processing the output would either depend on a proprietary device to transmit or a more mighty microcontroller to process.  
Unlikely to be chosen.

### Generic CMOS  
A generic CMOS camera similar to what was used in Embedded Systems would be a low cost means to acquire video however it would be a much lower resolution picture compared to a Go Pro, would not look as nice but also is less demanding in terms of bandwidth.  
High resolution is not necessary as the FPV must merely offer assistance in guiding towards the drop zone and offering a means for the judges to monitor the drop.  
OV7670 is one such CMOS module.  
Offer simple analog and digital outputs which could be passed directly to a transmitter or processed by a lightweight microcontroller.  
Will most likely be used.

### "Drone FPV  Kit"  
With the increasing popularity of quad copters and similar drones there are a number of drop in FPV systems available.  
This however seems to go against the idea of Senior Design and will least likely be chosen.
---
## Transmission/Reception
### 5.8Ghz A/V system
There are numerous systems operating in the 5.8Ghz band which take in an analog audio/video signal via component cables similar to a vcr or DVD player and simply recreate the signal on the other side with little intervention necessary. 
Bosscam is one such system with various power levels and ranges from 2km upward.  
~$50+  
Requires a device to convert analog video signal for consumption by pc.  
While the 5.8Ghz module does much of the work for us despite the goal of Senior Design being to come up with ways to do this on our own I feel this is acceptable to not entirely reinvent the wheel with a hacked together system via zigbee as it still requires a good bit of work to get going unlike a drop in FPV kit.

### Roll your own
With the use of the digital camera outputs and a microcontroller the images could be sent through a generic wireless interface similar to zigbee or xbee, however it must remain outside the 2.4Ghz band to comply with the rules.  
900Mhz zigbee exists.  
Need to further investigate bandwidth capacity depending on module chosen and bandwidth need s depending on camera chosen.  
Would require an additional controller to facilitate translation from camera to wireless module and wireless module to pc on the other side.  
Wide price range.

### "Drone FPV Kit"
With the increasing popularity of quad copters and similar drones there are a number of drop in FPV systems available. This however seems to go against the idea of Senior Design and will least likely be chosen.  

---
All systems require the development of an interface on the base station to display video in real time and the ability to save/replay past videos on demand.

---
As of right now I'm leaning towards using a generic CMOS camera module with the 5.8Ghz transmitter/receiver connected to a usb capture card on the computer which can be read by software written for the base station pc.